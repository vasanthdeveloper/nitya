// This file will handle deployment of the static site once generated

import fs from 'fs'
import path from 'path'

import ora from 'ora'
import chalk from 'chalk'

import { ConfigImpl, DeployProviderConfigImpl, getDomain } from '../config'
import { checkIfBuilt } from './serve'
import github, { GitHubConfigImpl } from '../deployments/github'
import { checkIfDeployable } from '../validate'

export async function writeCNAME(
    publicDirectory,
    options,
    config: ConfigImpl,
): Promise<void> {
    if (typeof getDomain(options, config) == 'string') {
        await fs.promises.writeFile(
            path.join(publicDirectory, 'CNAME'),
            getDomain(options, config),
            { encoding: 'utf8' },
        )
        return
    } else {
        return
    }
}

export async function deployStatic(
    publicDirectory: string,
    options,
    config: ConfigImpl,
    spinner: ora.Ora,
): Promise<void> {
    // 1. Deploy to the configured provider
    if (config.deploy.provider == 'gh-pages') {
        await github(publicDirectory, options, config, spinner)
    }
}

export default async function deploy(
    options,
    config: ConfigImpl,
): Promise<void> {
    // 1. Prepare all the required variables
    const publicDirectory: string = path.join(process.cwd(), 'public')

    // 2. Do all sorts of checks before we commit to building
    await checkIfDeployable(config)
    await checkIfBuilt(publicDirectory)

    // 2. Prepare a ora spinner
    const spinner = ora({
        color: 'cyan',
        hideCursor: true,
        isEnabled: true,
        text: 'Preparing to deploy static site',
        interval: 80,
    }).start()

    // 3. Deploy the static site accordingly
    await writeCNAME(publicDirectory, options, config)
    await deployStatic(publicDirectory, options, config, spinner)

    // 4. Stop the spinner
    spinner.stopAndPersist({
        symbol: chalk.greenBright('✔'),
        text: 'Finished deploying static site',
    })
    return
}
