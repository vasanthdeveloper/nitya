// This files handles the root command

import fs from 'fs'
import path from 'path'

import del from 'del'
import inquirer from 'inquirer'
import yaml from 'js-yaml'
import ora from 'ora'
import chalk from 'chalk'

import { ConfigImpl, defaultConfig } from '../config'
import errorHandler from '../errorHandler'
import logger from '../logger'
import {
    validateGhostURL,
    isGitURL,
    checkIfDeployable,
    validateURL,
} from '../validate'
import {
    handlePublicDirectory,
    fetchStatics,
    fixWgetLinks,
    minifyStaticSite,
} from '../cmd/build'
import { deployStatic, writeCNAME } from '../cmd/deploy'
import { checkIfBuilt } from './serve'

// makeConfigPath will return a full absolute path when passed with a filename with extension
function makeConfigPath(filename: string): string {
    return path.join(process.cwd(), filename)
}

async function createNewConfig(): Promise<void> {
    // 1. Prepare an array of all possible paths
    const alreadyExists = null
    const possiblePaths = [
        makeConfigPath('.nityarc.yml'),
        makeConfigPath('.nityarc.json'),
        makeConfigPath('nitya.config.js'),
    ]

    // 2. Loop through each one of them and check they exist
    for (const index in possiblePaths) {
        if (alreadyExists == null) {
            if (fs.existsSync(possiblePaths[index])) {
                // alreadyExists = possiblePaths[index]
                // Ask the user whether he wants to overwrite the existing config
                const overwritePrompt = await inquirer.prompt({
                    type: 'confirm',
                    name: 'overwrite',
                    message: `Found a ${path.basename(
                        possiblePaths[index],
                    )} file. Would you like to overwrite it?`,
                    default: false,
                })

                // Exit if the user didn't want to overwrite the file
                if (overwritePrompt.overwrite !== true) {
                    process.exit(3)
                }
            }
        }
    }

    // 3. Ask the user what type of config file he needs
    const configPrompts = await inquirer.prompt([
        {
            type: 'list',
            name: 'type',
            message: 'What type of config file should be created?',
            default: 2,
            choices: [
                {
                    title: 'YAML',
                    value: '.nityarc.yml',
                    // eslint-disable-next-line prettier/prettier
                    description: '(.nityarc.yml) YAML Ain\'t Markup Language',
                },
                {
                    title: 'JSON',
                    value: '.nityarc.json',
                    description: '(.nityarc.json) JavaScript object notation',
                },
                {
                    title: 'JavaScript',
                    value: 'nitya.config.js',
                    description: '(nitya.config.js) JavaScript',
                },
            ],
        },
        {
            type: 'input',
            name: 'uri',
            message: 'What is the URL of Ghost site?',
            validate: (value): boolean | string => {
                return validateGhostURL(value)
            },
        },
        {
            type: 'confirm',
            message: 'Would you like to minify the generated static site?',
            name: 'minify',
            default: false,
        },
        {
            type: 'rawlist',
            message: 'Where should this site be deployed to?',
            name: 'deploy',
            choices: [{ title: 'GitHub Pages', value: 'gh-pages' }],
        },
        {
            type: (prev): boolean | string =>
                prev == 'gh-pages' ? 'text' : false,
            message: 'What is the GitHub repository?',
            name: 'repository',
            validate: (value): boolean | string => {
                return isGitURL(value)
            },
        },
    ])

    // Create the file path accordingly
    const configFilePath: string = makeConfigPath(configPrompts.type)
    let configString: string

    // Create a new config object
    const configObject: ConfigImpl = {
        minify: configPrompts.minify,
        uri: configPrompts.uri,
        server: defaultConfig.server,
        deploy: {
            provider: configPrompts.deploy,
            repository: configPrompts.repository,
        },
    }

    // Prepare the config file contents accordingly
    if (configPrompts.type == '.nityarc.yml') {
        configString = yaml.safeDump(configObject, {
            indent: 4,
            lineWidth: 300,
        })
    } else if (configPrompts.type == '.nityarc.json') {
        configString = JSON.stringify(configObject, null, 4)
    } else if (configPrompts.type == 'nitya.config.js') {
        configString = `module.exports = ${JSON.stringify(
            configObject,
            null,
            4,
        )}`
    }

    // Delete existing configuration
    await del(possiblePaths)

    // Write the file
    fs.writeFileSync(configFilePath, configString, {
        encoding: 'UTF-8',
    })
    logger.success(
        `A ${path.basename(configFilePath)} is written to: ${process.cwd()}`,
    )
}

async function buildSite(options, config: ConfigImpl): Promise<void> {
    // 1. Prepare all the required variables
    const publicDirectory: string = path.join(process.cwd(), 'public')

    // 2. Do all sorts of checks before we commit to building
    await validateURL(config.uri)
    await handlePublicDirectory(publicDirectory, options, config)
    await checkIfDeployable(config)

    // 2. Prepare a ora spinner
    const spinner = ora({
        color: 'cyan',
        hideCursor: true,
        isEnabled: true,
        text: 'Preparing to build static site',
        interval: 80,
    }).start()

    await fetchStatics(config.uri, publicDirectory, spinner)
    spinner.text = 'Fixing asset links'
    await fixWgetLinks(publicDirectory)
    await minifyStaticSite(config.minify, publicDirectory, spinner)
    await writeCNAME(publicDirectory, options, config)
    await deployStatic(publicDirectory, options, config, spinner)

    spinner.stopAndPersist({
        symbol: chalk.greenBright('✔'),
        text: 'Finished building static site',
    })
}

export default async function root(options, config: ConfigImpl): Promise<void> {
    // Check if init is try
    if (options.init === true) {
        // Initialize a new config file with defaults and exit the program
        errorHandler(createNewConfig()).then(data => {
            if (data.error) {
                logger.error(data.error, 3)
            } else {
                process.exit(0)
            }
        })
    } else {
        // Check if we are loading from a config file
        if (typeof config.file == 'string') {
            // Check if there is a "uri" property specified in the config file
            if (typeof config.uri == 'string') {
                // Start building the static site
                return await buildSite(options, config)
            } else {
                // Tell the user that there isn't a "uri" specified and exit
                logger.error('No source "uri" specified.', 1)
            }
        } else {
            // Tell the user that there is nothing to do
            // as no automatic configuration was found
            // and show the help message
            logger.error('No automatic configuration found.', 2)
        }
    }
}
