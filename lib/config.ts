// This file will contain default config values as well as load the config file
// it reads and returns a configuration object if a config file is found in the current directory
// or else, just returns a default config with default values

import path from 'path'

import { cosmiconfig } from 'cosmiconfig'

import { GitHubConfigImpl } from '../lib/deployments/github'

// TODO: Add an enum to deploy provider filed
export interface DeployProviderConfigImpl {
    provider: string
    domain?: string
}

export const possibleDeploymentProviders: string[] = ['gh-pages']

export interface ConfigImpl {
    uri?: string
    file?: string
    minify: boolean
    server: {
        host: string
        port: number
    }
    deploy?: GitHubConfigImpl
}

export const defaultConfig: ConfigImpl = {
    uri: null,
    minify: false,
    deploy: null,
    server: {
        host: 'http://localhost',
        port: 2020,
    },
}

export default async function loadConfig(): Promise<ConfigImpl> {
    // 1. Prepare the cosmiconfig module
    const explorer = await cosmiconfig('nitya', {
        packageProp: 'nitya',
    })

    // 2. Search for a valid config file by walking up
    const configFile = await explorer.search()

    // If a config file was found
    if (configFile) {
        // 3. Assume defaults for unspecified properties in the config file
        for (const obj in defaultConfig) {
            if (configFile.config[obj] === undefined) {
                configFile.config[obj] = defaultConfig[obj]
            }
        }

        // 4. Delete all the keys that are not defined in the default configuration
        for (const obj in configFile.config) {
            if (defaultConfig[obj] === undefined) {
                throw new Error(
                    `Unrecognized configuration '${obj}' in ${path.basename(
                        configFile.filepath,
                    )}`,
                )
            }
        }

        // 5. Set the file path to config
        configFile.config.file = configFile.filepath

        // 6. Return the config file as a ConfigImpl
        return configFile.config as ConfigImpl
    } else {
        // 3. Simply, return a default config
        return defaultConfig
    }
}

export function getDomain(options, config: ConfigImpl): string {
    // 1. Check if options has a domain key with value
    if (typeof options.domain == 'string') {
        return options.domain
    } else {
        // 2. Check if we have a deployment settings
        if (config.deploy !== null) {
            // 3. Check if we have a domain in the config
            if (typeof config.deploy.domain == 'string') {
                return config.deploy.domain
            } else {
                return null
            }
        } else {
            return null
        }
    }
}
