// This file checks if a GitHub pages deployment can be made
// if everything checks out, we deploy to GitHub pages

import ora from 'ora'
import ghpages from 'gh-pages'

import { DeployProviderConfigImpl } from '../config'
import { isGitURL } from '../validate'
import logger from '../logger'
import { ConfigImpl } from '../config'

export interface GitHubConfigImpl extends DeployProviderConfigImpl {
    repository: string
    message?: string
}

export async function validateGitHubDeployment(
    deploySettings: GitHubConfigImpl,
): Promise<void> {
    // Check if a repository field is available with a valid github repository
    const repositoryURL: string | boolean =
        deploySettings.repository == '' ||
        typeof deploySettings.repository == 'undefined'
            ? 'The deployment provider "gh-pages" requires a repository field'
            : isGitURL(deploySettings.repository)
    if (typeof repositoryURL == 'string') {
        logger.error(repositoryURL, 7)
    }

    return
}

export default function github(
    publicDirectory: string,
    options,
    config: ConfigImpl,
    spinner: ora.Ora,
): Promise<void> {
    return new Promise(resolve => {
        // 1. Cast the generic DeployProviderConfigImpl into a GitHubConfigImpl
        const deploySettings: DeployProviderConfigImpl = config.deploy
        const githubDeployOptions: GitHubConfigImpl = deploySettings as GitHubConfigImpl

        // 1. Tell the user we have started deployment to GitHub Pages
        spinner.text = `Deploying the static site to ${deploySettings.provider}`

        // 2. Prepare the deployment options for gh-pages module
        const ghpagesOptions = {
            branch: 'gh-pages',
            repo: githubDeployOptions.repository,
            message: githubDeployOptions.message
                ? githubDeployOptions.message
                : '📗 Deployed static site by nitya',
            history: false,
        }

        // 4. Start publishing on GitHub Pages
        ghpages.publish(publicDirectory, ghpagesOptions, err => {
            if (err) {
                throw err
            } else {
                resolve()
            }
        })
    })
}
