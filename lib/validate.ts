// This file will contain validation functions used throughout the application

import validURL from 'url-validation'
import validGitURL from 'is-git-url'

import logger from './logger'
import { possibleDeploymentProviders } from './config'
import {
    validateGitHubDeployment,
    GitHubConfigImpl,
} from './deployments/github'
import { ConfigImpl } from './config'

export function validateGhostURL(uri: string): boolean | string {
    const isURL: boolean = validURL(uri)
    if (isURL == false) {
        return 'Invalid Ghost URL'
    } else {
        return true
    }
}

export function isGitURL(uri: string): boolean | string {
    const isGitURL: boolean = validGitURL(uri)
    if (isGitURL == false) {
        return 'Invalid Git repository URL'
    } else {
        return true
    }
}

// checkIfDeployable should validate prior to proceeding if all the data
// required for a successful deploy are available or defined
export async function checkIfDeployable(config: ConfigImpl): Promise<void> {
    if (config.deploy == null) {
        // Exit with error no deployment configured
        logger.error('No deployment configuration found', 6)
        return
    } else {
        // Now that we know, deploy key is there and not null
        // check if there is a deployment provider
        if (
            config.deploy.provider != null ||
            config.deploy.provider != undefined
        ) {
            // Check if the provider field includes a valid provider
            if (
                possibleDeploymentProviders.includes(config.deploy.provider) ==
                true
            ) {
                // 1. Act accordingly
                if (config.deploy.provider == 'gh-pages') {
                    validateGitHubDeployment(config.deploy as GitHubConfigImpl)
                }
            } else {
                console.log(config.deploy.provider)
                logger.error('Invalid deployment provider', 7)
            }
        } else {
            logger.error('No deployment provider configured', 7)
        }
    }
}

// validateURL is the function that ends the program when no or invalid URL is specified for the input
export async function validateURL(url: string): Promise<void> {
    if (validURL(url) == false) {
        logger.error('The provided target URL is invalid', 1)
    } else {
        return
    }
}
