import ora from 'ora';
import { ConfigImpl } from '../config';
export declare function fetchStatics(url: string, publicDirectory: string, spinner: ora.Ora): Promise<void>;
export declare function handlePublicDirectory(publicDirectory: string, options: any, config: ConfigImpl): Promise<void>;
export declare function fixWgetLinks(publicDirectory: string): Promise<void>;
export declare function minifyStaticSite(minify: boolean, publicDirectory: string, spinner: ora.Ora): Promise<void>;
export default function build(args: any, options: any, config: ConfigImpl): Promise<void>;
//# sourceMappingURL=build.d.ts.map