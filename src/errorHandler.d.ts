interface HandlerResponseImpl {
    error: Error | undefined;
    data: any;
}
export default function errorHandler(target: any): Promise<HandlerResponseImpl>;
export {};
//# sourceMappingURL=errorHandler.d.ts.map